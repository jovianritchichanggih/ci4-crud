<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

$routes->get('/', 'Home::index');
$routes->add('/login', 'Home::login');
$routes->add('/logout', 'Home::logout');

$routes->get('user', 'UserController::index', ['filter' => 'authfilter']);
$routes->add('user', 'UserController::create', ['filter' => 'authfilter']);
$routes->add('user/edit/(:segment)', 'UserController::edit/$1', ['filter' => 'authfilter']);
$routes->add('user/delete/(:segment)', 'UserController::delete/$1', ['filter' => 'authfilter']);

$routes->get('employee', 'EmployeeController::index', ['filter' => 'authfilter']);
$routes->add('employee', 'EmployeeController::create', ['filter' => 'authfilter']);
$routes->add('employee/edit/(:segment)', 'EmployeeController::edit/$1', ['filter' => 'authfilter']);
$routes->add('employee/delete/(:segment)', 'EmployeeController::delete/$1', ['filter' => 'authfilter']);
