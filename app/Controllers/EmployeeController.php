<?php

namespace App\Controllers;
use App\Models\Employee;

class EmployeeController extends BaseController
{
    protected $employee;
    function __construct()
    {
        $this->employee = new Employee();
    }

    public function index()
    {
	    $data['employees'] = $this->employee->paginate(10, 'employee');
        $data['pager'] = $this->employee->pager;
        $data['educations'] = $this->employee->getEducationList();
        $data['sexes'] = $this->employee->getSexList();
        return view('employees/index', $data);
    }

    public function create()
    {
        $request = $this->request->getPost();
        $img = $this->request->getFile('photo');
        $name = $request['idno'] . "." . $img->getClientExtension();

        /* IF EXIST THEN REMOVE */
        if(file_exists(FCPATH . "uploads\\" . $name)){
            unlink(FCPATH . "uploads\\" . $name);
        }

        /* SAVE PHOTO TO FOLDER */
        if (!$img->hasMoved()){
            $img->move('uploads/', $name);
        }

        $this->employee->insert([
            'employee_no' => $this->autoGenerate(),
            'id_no' => $request['idno'],
            'firstname' => $request['firstname'],
            'lastname' => $request['lastname'],
            'phone_no' => $request['phone'],
            'address' => $request['address'],
            'hired_date' => $request['hired'],
            'ed_level' => $request['ed'],
            'sex' => $request['sex'],
            'birthdate' => $request['birthdate'],
            'salary' => $request['salary'],
            'photo' => $name,
        ]);
        return redirect('employee')->with('success', 'Berhasil menyimpan pegawai baru.');
    }

    public function edit($id)
    {
        $request = $this->request->getPost();
        $img = $this->request->getFile('photo');

        if(!empty($img->getPath())){
            $name = $request['idno'] . "." . $img->getClientExtension();
            /* IF EXIST THEN REMOVE */
            if(file_exists(FCPATH . "uploads\\" . $name)){
                unlink(FCPATH . "uploads\\" . $name);
            }

            /* SAVE PHOTO TO FOLDER */
            if (!$img->hasMoved()){
                $img->move('uploads/', $name);
            }
        }

        $this->employee->update($id, [
            'id_no' => $request['idno'],
            'firstname' => $request['firstname'],
            'lastname' => $request['lastname'],
            'phone_no' => $request['phone'],
            'address' => $request['address'],
            'hired_date' => $request['hired'],
            'ed_level' => $request['ed'],
            'sex' => $request['sex'],
            'birthdate' => $request['birthdate'],
            'salary' => $request['salary'],
        ]);

        if(!empty($img->getPath())){
            $this->employee->update($id, ['photo' => $name]);
        }
        return redirect('employee')->with('success', 'Berhasil mengubah data pegawai.');
    }

    public function delete($id)
    {
        $employee = $this->employee->find($id);
        if(file_exists(FCPATH . "uploads\\" . $employee['photo'])){
            unlink(FCPATH . "uploads\\" . $employee['photo']);
        }
        $this->employee->delete($id);
        return redirect('employee')->with('success', 'Berhasil menghapus pegawai.');
    }

    function autoGenerate()
    {
        $prefix = "EMP";
        $number = 1;

        /* FIRST ENTRY */
        $latest = $this->employee->selectMax("employee_no")->find();
        if($latest[0]["employee_no"]){
            $number = substr($latest[0]["employee_no"], -3, 3) + 1;
        }
        return $prefix . "/" . str_pad($number, 3, "0", STR_PAD_LEFT);
    }
}