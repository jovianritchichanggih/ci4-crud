<?php

namespace App\Controllers;
use App\Models\User;

class Home extends BaseController
{
    protected $user;
    function __construct()
    {
        $this->user = new User();
    }

    public function index()
    {
        $session = session();
        if($session->get("logged_in")){
            return redirect()->to('user');
        }

        return view('login');
    }

    public function login()
    {
        $session = session();
        $request = $this->request->getPost();
        $user = $this->user->where("e-mail", $request['username'])->find();

        /* IF SUPERUSER */
        if($request['username'] == "admin" && $request['password'] == "admin"){
            $sess = ['email' => $request['username'], 'logged_in' => true];
            $session->set($sess);
            return redirect('user')->with('success', 'Login sukses!');
        }

        /* IF NOT SUPERUSER */
        if(md5($request['password']) != $user[0]['password']){
            return redirect('/')->with('error', 'E-mail / Password salah');
        }else{
            $sess = ['email' => $user[0]['e-mail'],'logged_in' => true];
            $session->set($sess);
            $this->user->update($user[0]['id'], ['last_login' => date("Y-m-d H:i:")]);
            return redirect('user')->with('success', 'Login sukses!');
        }
    }

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/');
    }
}
