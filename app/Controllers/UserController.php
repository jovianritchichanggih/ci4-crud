<?php

namespace App\Controllers;
use App\Models\User;

class UserController extends BaseController
{
    protected $user;
    function __construct()
    {
        $this->user = new User();
    }

    public function index()
    {
	    $data['users'] = $this->user->where('status', User::STATUS_ACTIVE)->paginate(10, 'user');
        $data['pager'] = $this->user->pager;
        return view('users/index', $data);
    }

    public function create()
    {
        $request = $this->request->getPost();
        $user = $this->user->where("e-mail", $request['email'])->find();

        if(count($user) > 0){
            return redirect('user')->with('error', 'E-mail sudah pernah dipakai.');
        }

        if($request['password'] !== $request['password2']){
            return redirect('user')->with('error', 'Password dan Ulangi Password harus sama.');
        }else{
            $hashed = md5($request['password']);
            $this->user->insert([
                'username' => $request['name'],
                'e-mail' => $request['email'],
                'password' => $hashed,
                'status' => User::STATUS_ACTIVE,
                'last_login' => date("Y-m-d H:i:s"),
            ]);
            return redirect('user')->with('success', 'Berhasil menyimpan user baru.');
        }
    }

    public function edit($id)
    {
        $request = $this->request->getPost();
        $user = $this->user->find($id);

        if($request['password2'] !== $request['password3']){
            return redirect('user')->with('error', 'Password dan Ulangi Password Baru harus sama.');
        }else{
            $hashed = md5($request['password']);
            if($hashed !== $user['password']){
                return redirect('user')->with('error', 'Password Lama salah! Coba lagi!');
            }else{
                $this->user->update($id, ['password' => md5($request['password3'])]);
                return redirect('user')->with('success', 'Berhasil mengubah password user.');
            }
        }
    }

    public function delete($id)
    {
        $this->user->update($id, ['status' => User::STATUS_INACTIVE]);
        return redirect('user')->with('success', 'Berhasil menghapus user.');
    }
}