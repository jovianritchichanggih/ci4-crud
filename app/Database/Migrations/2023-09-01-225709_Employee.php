<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Employee extends Migration
{
    public function up()
    {
        ['employee_no', 'id_no', 'firstname', 'lastname', 'phone_no', 'address', 'hired_date', 'ed_level', 'sex', 'birthdate', 'salary'];
        $this->forge->addField([
            'id' => [
                'type'  => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
            ],
            'employee_no' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'id_no' => [
                'type' => 'VARCHAR',
                'constraint' => 16,
            ],
            'firstname' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'lastname' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'phone_no' => [
                'type' => 'VARCHAR',
                'constraint' => 15,
            ],
            'address' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'hired_date' => [
                'type' => 'DATE',
            ],
            'ed_level' => [
                'type' => 'INT',
                'constraint' => 2,
            ],
            'sex' => [
                'type' => 'INT',
                'constraint' => 1,
            ],
            'birthdate' => [
                'type' => 'DATE',
            ],
            'salary' => [
                'type' => 'FLOAT',
                'constraint' => 15,
            ],
            'photo' => [
                'type' => 'VARCHAR',
                'constraint' => 50,
            ]
        ]);

        $this->forge->addKey('id', TRUE);
        $this->forge->createTable('employee');
    }

    public function down()
    {
        $this->forge->dropTable('employee');
    }
}
