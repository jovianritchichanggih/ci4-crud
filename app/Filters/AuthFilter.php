<?php
    namespace App\Filters;
    use CodeIgniter\HTTP\RequestInterface;
    use CodeIgniter\HTTP\ResponseInterface;
    use CodeIgniter\Filters\FilterInterface;

    class AuthFilter implements FilterInterface {
        public function before(RequestInterface $request, $arguments = null){
            if (!session()->get("logged_in")){
                return redirect()->to(base_url('/logout'))->with('error', "Mohon login terlebih dahulu.");
	        }
        }

        public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)   {
            // Do something here
        }
    }
?>