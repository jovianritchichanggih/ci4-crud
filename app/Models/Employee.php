<?php

namespace App\Models;
use CodeIgniter\Model;

class Employee extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'employee';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['employee_no', 'id_no', 'firstname', 'lastname', 'phone_no', 'address', 'hired_date', 'ed_level', 'sex', 'birthdate', 'salary', 'photo'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    const ED_PRIMARY = 1;
    const ED_MIDDLE = 2;
    const ED_HIGH = 3;
    const ED_DIPLOMA = 4;
    const ED_BACHELORS = 5;
    const ED_MASTERS = 6;

    const SEX_MAN = 1;
    const SEX_WOMAN = 2;

    public static function getEducationList(){
        return array(
            self::ED_PRIMARY => "Sekolah Dasar",
            self::ED_MIDDLE => "Sekolah Menengah Pertama",
            self::ED_HIGH => "Sekolah Menengah Atas",
            self::ED_DIPLOMA => "Diploma",
            self::ED_BACHELORS => "S1",
            self::ED_MASTERS => "S2",
        );
    }

    public static function getSexList(){
        return array(
            self::SEX_MAN => "Pria",
            self::SEX_WOMAN => "Wanita",
        );
    }
}
