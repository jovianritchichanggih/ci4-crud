<?= $this->extend('layout/main_layout') ?>

<?= $this->section('content') ?>
    <?php helper('form'); ?>
    <div class="container-fluid p-2">
        <h1 class="h3 mb-2 text-gray-800 col-lg-8 offset-lg-2">Daftar Pegawai</h1>
        <div class="row mx-0">
            <div class="col-lg-8 offset-lg-2">
                <?php if(session()->getFlashData('success')): ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?= session()->getFlashData('success') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                <?php endif; ?>
                <?php if(session()->getFlashData('error')): ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <?= session()->getFlashData('error') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                <?php endif; ?>
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal"><i class='fas fa-sm fa-plus'></i> Pegawai Baru</button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="15%">NIK</th>
                                        <th width="15%">No. KTP</th>
                                        <th width="15%">Nama Lengkap</th>
                                        <th width="15%">No. Telp</th>
                                        <th width="15%">Alamat</th>
                                        <th width="10%">Tanggal Lahir</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($employees as $index => $employee) : ?>
                                        <tr>
                                            <td><?= ++$index ?></td>
                                            <td data-toggle="modal" data-target="#viewModal-<?= $employee['id'] ?>" style='cursor:pointer;'><?= $employee['employee_no'] ?></td>
                                            <td><?= $employee['id_no'] ?></td>
                                            <td><?= $employee['firstname'] . " " . $employee['lastname'] ?></td>
                                            <td><?= $employee['phone_no'] ?></td>
                                            <td><?= $employee['address'] ?></td>
                                            <td><?= $employee['hired_date'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal-<?= $employee['id'] ?>"><i class="fas fa-sm fa-edit"></i></button>
                                                <a href="<?= base_url('employee/delete/'.$employee['id']) ?>" class="btn btn-danger" onclick="return confirm('Are you sure ?')"><i class="fas fa-sm fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <!-- Edit Modal -->
                                        <div class="modal fade" id="editModal-<?= $employee['id'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Pegawai</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    </div>
                                                    <form action="<?= base_url('employee/edit/'.$employee['id']) ?>" method="post" enctype="multipart/form-data">
                                                        <?= csrf_field(); ?>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="idno">No. KTP</label>
                                                                <input type="text" name="idno" class="form-control" id="idno" placeholder="no. ktp (16 angka)" pattern="[0-9]{16}" value="<?= $employee['id_no']; ?>" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="firstname">Nama Depan</label>
                                                                <input type="text" name="firstname" class="form-control" id="firstname" placeholder="nama depan" value="<?= $employee['firstname'] ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="lastname">Nama Belakang</label>
                                                                <input type="text" name="lastname" class="form-control" id="lastname" placeholder="nama belakang" value="<?= $employee['lastname'] ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="phone">No. Telp</label>
                                                                <input type="tel" name="phone" class="form-control" id="phone" placeholder="cth : 081123456789"  pattern="[0-9]{3,4}[0-9]{1,10}" value="<?= $employee['phone_no']; ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="address">Alamat</label>
                                                                <input type="text" name="address" class="form-control" id="address" placeholder="alamat" value="<?= $employee['address'] ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="hired">Tanggal Masuk</label>
                                                                <input type="date" name="hired" class="form-control" id="hired" placeholder="tanggal masuk"  max="<?php echo date("Y-m-d", strtotime('+60 days')); ?>" value="<?= $employee['hired_date']; ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="ed">Edukasi Terakhir</label>
                                                                <?php echo form_dropdown('ed', $educations, $employee['ed_level'], ['id' => 'ed', 'required', 'class' => 'form-control']); ?>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="ed">Jenis Kelamin</label>
                                                                <?php echo form_dropdown('sex', $sexes, $employee['sex'], ['id' => 'sex', 'required', 'class' => 'form-control']); ?>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="birthdate">Tanggal Lahir</label>
                                                                <input type="date" name="birthdate" class="form-control" id="birthdate" placeholder="tanggal lahir" max="<?php echo date("Y-m-d", strtotime('-17 years')); ?>" value="<?= $employee['birthdate']; ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="salary">Gaji Terakhir</label>
                                                                <input type="number" name="salary" class="form-control" id="salary" placeholder="gaji terakhir" value="<?= $employee['salary'] ?>" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="photo">Foto</label>
                                                                <?= form_upload('photo', '', ['class' => 'form-control-file', 'id' => 'photo', 'accept' => 'image/jpg, image/jpeg']); ?>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- View Modal -->
                                        <div class="modal fade" id="viewModal-<?= $employee['id'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit Pegawai</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    </div>
                                                    <form action="<?= base_url('employee/edit/'.$employee['id']) ?>" method="post">
                                                        <?= csrf_field(); ?>
                                                        <div class="modal-body">
                                                            <img src="<?= base_url() . "uploads/" . $employee['photo']; ?>" class="img-thumbnail"/></br>
                                                            <?= "NIK : " . $employee['employee_no']; ?><br/>
                                                            <?= "No. KTP : " . $employee['id_no']; ?><br/>
                                                            <?= "Nama Lengkap : " . $employee['firstname'] . " " . $employee['lastname']; ?><br/>
                                                            <?= "No. Telp : " . $employee['phone_no']; ?><br/>
                                                            <?= "Address  : " . $employee['address']; ?><br/>
                                                            <?= "Tanggal Lahir : " . $employee['birthdate']; ?><br/>
                                                            <?= "Tanggal Masuk : " . $employee['hired_date']; ?><br/>
                                                            <?= "Edukasi Terakhir : " . $educations[$employee['ed_level']]; ?><br/>
                                                            <?= "Jenis Kelamin : " . $sexes[$employee['sex']]; ?><br/>
                                                            <?= "Gaji Terakhir : " . number_format($employee['salary'], 0); ?><br/>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?= $pager->links('employee','bootstrap_pagination') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Pegawai Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form action="<?= base_url('employee') ?>" method="post" enctype="multipart/form-data">
                    <?= csrf_field(); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="idno">No. KTP</label>
                            <input type="text" name="idno" class="form-control" id="idno" placeholder="no. ktp (16 angka)" pattern="[0-9]{16}" required>
                        </div>
                        <div class="form-group">
                            <label for="firstname">Nama Depan</label>
                            <input type="text" name="firstname" class="form-control" id="firstname" placeholder="nama depan" required>
                        </div>
                        <div class="form-group">
                            <label for="lastname">Nama Belakang</label>
                            <input type="text" name="lastname" class="form-control" id="lastname" placeholder="nama belakang" required>
                        </div>
                        <div class="form-group">
                            <label for="phone">No. Telp</label>
                            <input type="tel" name="phone" class="form-control" id="phone" placeholder="cth : 081123456789"  pattern="[0-9]{3,4}[0-9]{1,10}" required>
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat</label>
                            <input type="text" name="address" class="form-control" id="address" placeholder="alamat" required>
                        </div>
                        <div class="form-group">
                            <label for="hired">Tanggal Masuk</label>
                            <input type="date" name="hired" class="form-control" id="hired" placeholder="tanggal masuk"  max="<?php echo date("Y-m-d", strtotime('+60 days')); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="ed">Edukasi Terakhir</label>
                            <?php echo form_dropdown('ed', $educations, '', ['id' => 'ed', 'required', 'class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <label for="ed">Jenis Kelamin</label>
                            <?php echo form_dropdown('sex', $sexes, '', ['id' => 'sex', 'required', 'class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <label for="birthdate">Tanggal Lahir</label>
                            <input type="date" name="birthdate" class="form-control" id="birthdate" placeholder="tanggal lahir" max="<?php echo date("Y-m-d", strtotime('-17 years')); ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="salary">Gaji Terakhir</label>
                            <input type="number" name="salary" class="form-control" id="salary" placeholder="gaji terakhir" required>
                        </div>
                        <div class="form-group">
                            <label for="photo">Foto</label>
                            <?= form_upload('photo', '', ['class' => 'form-control-file', 'id' => 'photo', 'accept' => 'image/jpg, image/jpeg']); ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <style>
        th {
            vertical-align: middle !important;
            text-align: center;
        }
    </style>
    <script>
        var uploadField = document.getElementById("photo");
        uploadField.onchange = function() {
            if(this.files[0].size > 307200){
                alert("Maksimal ukuran file adalah 300 KB.");
                this.value = "";
            };
        };
    </script>
<?= $this->endSection() ?>