<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CI4 CRUD</title>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>

        <style {csp-style-nonce}>
                * {
                    transition: background-color 300ms ease, color 300ms ease;
                }
                *:focus {
                    background-color: rgba(221, 72, 20, .2);
                    outline: none;
                }
                html, body {
                    color: rgba(33, 37, 41, 1);
                    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji";
                    font-size: 16px;
                    margin: 0;
                    padding: 0;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    text-rendering: optimizeLegibility;
                }
                header {
                    background-color: rgba(247, 248, 249, 1);
                    padding: .4rem 0 0;
                }
                .menu {
                    padding: .4rem 2rem;
                }
                header ul {
                    border-bottom: 1px solid rgba(242, 242, 242, 1);
                    list-style-type: none;
                    margin: 0;
                    overflow: hidden;
                    padding: 0;
                    text-align: right;
                }
                header li {
                    display: inline-block;
                }
                header li a {
                    border-radius: 5px;
                    color: rgba(0, 0, 0, .5);
                    display: block;
                    height: 44px;
                    text-decoration: none;
                }
                header li.menu-item a {
                    border-radius: 5px;
                    margin: 5px 0;
                    height: 38px;
                    line-height: 36px;
                    padding: .4rem .65rem;
                    text-align: center;
                }
                header li.menu-item a:hover,
                header li.menu-item a:focus {
                    background-color: rgba(221, 72, 20, .2);
                    color: rgba(221, 72, 20, 1);
                }
                header .logo {
                    float: left;
                    height: 44px;
                    padding: .4rem .5rem;
                }
                header .menu-toggle {
                    display: none;
                    float: right;
                    font-size: 2rem;
                    font-weight: bold;
                }
                header .menu-toggle button {
                    background-color: rgba(221, 72, 20, .6);
                    border: none;
                    border-radius: 3px;
                    color: rgba(255, 255, 255, 1);
                    cursor: pointer;
                    font: inherit;
                    font-size: 1.3rem;
                    height: 36px;
                    padding: 0;
                    margin: 11px 0;
                    overflow: visible;
                    width: 40px;
                }
                header .menu-toggle button:hover,
                header .menu-toggle button:focus {
                    background-color: rgba(221, 72, 20, .8);
                    color: rgba(255, 255, 255, .8);
                }
                header .heroe {
                    margin: 0 auto;
                    max-width: 1100px;
                    padding: 1rem 1.75rem 1.75rem 1.75rem;
                }
                header .heroe h1 {
                    font-size: 2.5rem;
                    font-weight: 500;
                }
                header .heroe h2 {
                    font-size: 1.5rem;
                    font-weight: 300;
                }
                section {
                    margin: 0 auto;
                    max-width: 1100px;
                    padding: 2.5rem 1.75rem 3.5rem 1.75rem;
                }
                section h1 {
                    margin-bottom: 2.5rem;
                }
                section h2 {
                    font-size: 120%;
                    line-height: 2.5rem;
                    padding-top: 1.5rem;
                }
                section pre {
                    background-color: rgba(247, 248, 249, 1);
                    border: 1px solid rgba(242, 242, 242, 1);
                    display: block;
                    font-size: .9rem;
                    margin: 2rem 0;
                    padding: 1rem 1.5rem;
                    white-space: pre-wrap;
                    word-break: break-all;
                }
                section code {
                    display: block;
                }
                section a {
                    color: rgba(221, 72, 20, 1);
                }
                section svg {
                    margin-bottom: -5px;
                    margin-right: 5px;
                    width: 25px;
                }
                .further {
                    background-color: rgba(247, 248, 249, 1);
                    border-bottom: 1px solid rgba(242, 242, 242, 1);
                    border-top: 1px solid rgba(242, 242, 242, 1);
                }
                .further h2:first-of-type {
                    padding-top: 0;
                }
                footer {
                    background-color: rgba(221, 72, 20, .8);
                    text-align: center;
                }
                footer .environment {
                    color: rgba(255, 255, 255, 1);
                    padding: 2rem 1.75rem;
                }
                footer .copyrights {
                    background-color: rgba(62, 62, 62, 1);
                    color: rgba(200, 200, 200, 1);
                    padding: .25rem 1.75rem;
                }
                @media (max-width: 629px) {
                    header ul {
                        padding: 0;
                    }
                    header .menu-toggle {
                        padding: 0 1rem;
                    }
                    header .menu-item {
                        background-color: rgba(244, 245, 246, 1);
                        border-top: 1px solid rgba(242, 242, 242, 1);
                        margin: 0 15px;
                        width: calc(100% - 30px);
                    }
                    header .menu-toggle {
                        display: block;
                    }
                    header .hidden {
                        display: none;
                    }
                    header li.menu-item a {
                        background-color: rgba(221, 72, 20, .1);
                    }
                    header li.menu-item a:hover,
                    header li.menu-item a:focus {
                        background-color: rgba(221, 72, 20, .7);
                        color: rgba(255, 255, 255, .8);
                    }
                }
            </style>

    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
            <div class="container">
                <?php $session = session(); ?>
                <?php if(!$session->get("logged_in")): ?>
                    <a class="navbar-brand" href="<?= base_url() ?>">Home</a>
                <?php else: ?>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url('user') ?>">Master User</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= base_url('employee') ?>">Master Pegawai</a>
                            </li>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
            <?php if($session->get("logged_in")): ?>
                <a class="nav-item nav-link py-2 bg-light" href="<?= base_url('logout') ?>" style='border-radius:4px;'>Logout</a>
            <?php endif; ?>
        </nav>

        <?= $this->renderSection('content') ?>

        <footer class="footer mt-0 mb-0 bg-light">
            <div class="container fixed-bottom">Copyright &copy <?= Date('Y') ?> Jovian Ritchi Changgih</div>
        </footer>
    </body>
</html>