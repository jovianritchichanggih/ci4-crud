<?= $this->extend('layout/main_layout') ?>

<?= $this->section('content') ?>
<main class="form-signin">
    <div class="row">
        <div class="col-lg-4 offset-lg-4">
            <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error'); ?>
                </div>
            <?php endif; ?>
            <form method="post" action="<?= base_url(); ?>login">
                <?= csrf_field(); ?>
                <h1 class="h3 mb-3 fw-normal">Login</h1>
                <input type="text" name="username" id="username" placeholder="E-mail" class="form-control" required autofocus>
                <input type="password" name="password" id="password" placeholder="Password" class="form-control" required>
                <button type="submit" class="w-100 btn btn-lg btn-primary">Login</button>
            </form>
        </div>
    </div>
</main>
<?= $this->endSection() ?>