<?= $this->extend('layout/main_layout') ?>

<?= $this->section('content') ?>
    <h1 class="h3 mb-2 text-gray-800 col-lg-8 offset-lg-2">Daftar User</h1>
    <div class="container-fluid p-2">
        <div class="row mx-0">
            <div class="col-lg-8 offset-lg-2">
                <?php if(session()->getFlashData('success')): ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?= session()->getFlashData('success') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                <?php endif; ?>
                <?php if(session()->getFlashData('error')): ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <?= session()->getFlashData('error') ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                <?php endif; ?>
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal"><i class='fas fa-sm fa-plus'></i> User Baru</button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th width="3%">#</th>
                                        <th width="30%">Username</th>
                                        <th width="30%">Email</th>
                                        <th width="25%">Login Terakhir</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($users as $index => $user) : ?>
                                        <tr>
                                            <td><?= ++$index ?></td>
                                            <td><?= $user['username'] ?></td>
                                            <td><?= $user['e-mail'] ?></td>
                                            <td><?= $user['last_login'] ?></td>
                                            <td>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal-<?= $user['id'] ?>"><i class="fas fa-sm fa-edit"></i></button>
                                                <a href="<?= base_url('user/delete/'.$user['id']) ?>" class="btn btn-danger" onclick="return confirm('Are you sure ?')"><i class="fas fa-sm fa-trash"></i></a>
                                            </td>
                                        </tr>

                                        <!-- Edit Modal -->
                                        <div class="modal fade" id="editModal-<?= $user['id'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    </div>
                                                    <form action="<?= base_url('user/edit/'.$user['id']) ?>" method="post">
                                                        <?= csrf_field(); ?>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="name">Username</label>
                                                                <input type="text" name="name" class="form-control" id="name" value="<?= $user['username'] ?>" placeholder="username" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="email">Email</label>
                                                                <input type="email" name="email" class="form-control" id="email" value="<?= $user['e-mail'] ?>"  placeholder="e-mail" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="password">Password Lama</label>
                                                                <input type="password" name="password" class="form-control" id="password" placeholder="password lama" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="password2">Password Baru</label>
                                                                <input type="password" name="password2" class="form-control" id="password2" placeholder="password baru" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="password3">Ulangi Password Baru</label>
                                                                <input type="password" name="password3" class="form-control" id="password3" placeholder="ulangi password baru" required>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <?= $pager->links('user','bootstrap_pagination') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah User Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <form action="<?= base_url('user') ?>" method="post">
                    <?= csrf_field(); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Username</label>
                            <input type="text" name="name" class="form-control" id="name" placeholder="username" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="e-mail" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="form-control" id="password" placeholder="password" required>
                        </div>
                        <div class="form-group">
                            <label for="password2">Ulangi Password</label>
                            <input type="password" name="password2" class="form-control" id="password2" placeholder="ulangi password" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>